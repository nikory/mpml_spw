#ifndef GETPUT_H
#define GETPUT_H

template<typename Tvleft, typename Tvright>
void getput_templ(void* l, void* r, void* scale = nullptr, void* shift = nullptr)
{
    if(scale == nullptr)
    {
        if(shift == nullptr)
        {
            *(Tvleft*)l = *(Tvright*)r;
        }
        else
        {
            *(Tvleft*)l = *(Tvright*)r + *(Tvright*)shift;
        }
    }
    else
    {
        if(shift == nullptr)
        {
            *(Tvleft*)l = *(Tvright*)r * *(Tvright*)scale;
        }
        else
        {
            *(Tvleft*)l = *(Tvright*)r * *(Tvright*)scale + *(Tvright*)shift;
        }
    }
    
}

typedef void (*getput_t)(void*, void*, void*, void*);

getput_t getput(const char* rule)
{
    if(strcmp(rule,"bool->bool") == 0)
    {
        return getput_templ<bool,bool>;
    }
    if(strcmp(rule,"bool->char") == 0)
    {
        return getput_templ<char,bool>;
    }
    if(strcmp(rule,"bool->int") == 0)
    {
        return getput_templ<int,bool>;
    }
    if(strcmp(rule,"bool->float") == 0)
    {
        return getput_templ<float,bool>;
    }
    if(strcmp(rule,"bool->double") == 0)
    {
        return getput_templ<double,bool>;
    }
    if(strcmp(rule,"char->bool") == 0)
    {
        return getput_templ<bool,char>;
    }
    if(strcmp(rule,"char->char") == 0)
    {
        return getput_templ<char,char>;
    }
    if(strcmp(rule,"char->int") == 0)
    {
        return getput_templ<int,char>;
    }
    if(strcmp(rule,"char->float") == 0)
    {
        return getput_templ<float,char>;
    }
    if(strcmp(rule,"char->double") == 0)
    {
        return getput_templ<double,char>;
    }
    if(strcmp(rule,"int->bool") == 0)
    {
        return getput_templ<bool,int>;
    }
    if(strcmp(rule,"int->char") == 0)
    {
        return getput_templ<char,int>;
    }
    if(strcmp(rule,"int->int") == 0)
    {
        return getput_templ<int,int>;
    }
    if(strcmp(rule,"int->float") == 0)
    {
        return getput_templ<float,int>;
    }
    if(strcmp(rule,"int->double") == 0)
    {
        return getput_templ<double,int>;
    }
    if(strcmp(rule,"float->bool") == 0)
    {
        return getput_templ<bool,float>;
    }
    if(strcmp(rule,"float->char") == 0)
    {
        return getput_templ<char,float>;
    }
    if(strcmp(rule,"float->int") == 0)
    {
        return getput_templ<int,float>;
    }
    if(strcmp(rule,"float->float") == 0)
    {
        return getput_templ<float,float>;
    }
    if(strcmp(rule,"float->double") == 0)
    {
        return getput_templ<double,float>;
    }
    if(strcmp(rule,"double->bool") == 0)
    {
        return getput_templ<bool,double>;
    }
    if(strcmp(rule,"double->char") == 0)
    {
        return getput_templ<char,double>;
    }
    if(strcmp(rule,"double->int") == 0)
    {
        return getput_templ<int,double>;
    }
    if(strcmp(rule,"double->float") == 0)
    {
        return getput_templ<float,double>;
    }
    if(strcmp(rule,"double->double") == 0)
    {
        return getput_templ<double,double>;
    }
    return nullptr;
}

#include <giw_vars.h>
#include <map>
#include <string>

struct Tgetput_el
{
    varInfo* to = nullptr;
    varInfo* from = nullptr;
    char scale_buf[16];
    char shift_buf[16];
    void* scale = nullptr;
    void* shift = nullptr;
    getput_t assign;
    Tgetput_el()
    {
        memset(scale_buf,0,sizeof(scale_buf));
        memset(shift_buf,0,sizeof(shift_buf));
    }
    Tgetput_el(const Tgetput_el& el)
    {
        memcpy(this,&el,sizeof(Tgetput_el));
        if(scale != nullptr)
        {
            scale = scale_buf;
        }
        if(shift != nullptr)
        {
            shift = shift_buf;
        }
    }
    void operator()()
    {
        assign(to->addr,from->addr, scale, shift);
    }
    bool check()
    {
        bool res = (to != nullptr && from != nullptr && assign != nullptr);
        return res;
    }
};

const char* get_ctype(const varInfo* v)
{
    using namespace std;
    static map<int,map<int,string> > ctypes;
    if(ctypes.empty())
    {
        ctypes[tCH][1] = "char";
        ctypes[tIN][2] = "short";
        ctypes[tIN][4] = "int";
        ctypes[tRE][4] = "float";
        ctypes[tRE][8] = "double";
        ctypes[tLO][1] = "bool";
    }
    auto ty = ctypes.find(v->typ);
    if(ty == ctypes.end())
    {
        return nullptr;
    }
    auto res = ty->second.find(v->lng);
    if(res == ty->second.end())
    {
        return nullptr;
    }
    return res->second.c_str();
}

#endif