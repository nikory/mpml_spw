#ifndef YAML_TEMPL_H
#define YAML_TEMPL_H

#include <yaml-cpp/yaml.h>
#include <string>
#include <vector>
#include "cpptempl.h"

namespace YAML_templ
{

std::string parse(std::string yfile)
{
    using namespace std;
    YAML::Node root = YAML::LoadFile(yfile.c_str());
    YAML::Emitter out;
//    out.WriteStreamable("#aaa");
    out<<YAML::BeginMap;
    for(auto n: root)
    {
        //cout<<n.first.as<string>()<<endl;
        const YAML::Node templ = n.second["template"];
        if(templ)
        {
            //Делаем обработку узла-шаблона
            //cout<<"Template node"<<endl;
            //Обработка шаблона
            //Берем перечень аргументов
            vector<string> args_names = templ.as<vector<string>>();
            //Берем текст шаблона
            string pattern = n.second["pattern"].as<string>();
            //Создание экземпляров
            for(auto varg: n.second["instances"])
            {
                vector<string> iarg = varg.as<vector<string>>();
                cpptempl::data_map DataMap;
                for(int i=0;i<iarg.size();++i)
                {
                    DataMap[args_names[i]] = iarg[i];
                }
                auto outNode=YAML::Load(cpptempl::parse(pattern,DataMap));
                for(auto k: outNode)
                {
                    out<<YAML::Key<<k.first.as<string>();
                    out<<k.second;
                }
            }
        }
        else
        {
            //Пропускаем узел
            //cout<<"Skip node"<<endl;
            out<<YAML::Key<<n.first.as<string>();
            out<<n.second;
        }
    }
    out<<YAML::EndMap;
    return out.c_str();
}

}

#endif