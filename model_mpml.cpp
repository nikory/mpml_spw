#include <iostream>
#include <iomanip>
#include <mdl.h>
#include <namelist.h>
#include <giw_model.h>
#include <cmd_base.h>
#include <addvpt.h>
#include <model_obj.h>
#include <sstream>
#include <fstream>
#include <map>
#include <zmq_model_shlib.h>
//#include <stp_util.h>
//#include <cmd_base.h>
//#include <splitpath.h>
#include <Parser.h>
#include <Scanner.h>
#include <yaml-cpp/yaml.h>
#include <direct.h>
#include <h5i.h>
#include <string>
#include <giw_c_loader.h>
#include <giw_iface.h>
#include "giw_error_level.h"
#include "mpml_data_block.h"
#include <fstream>
#include "getput.h"
#include <giw_vars.h>

//#include <iostream>
//#include <fstream>
//#include <iomanip>
//#include "zmq_script_giw.h"

using namespace std;

#define pintegr _pintegr

static float deltat;
const int buf_size = 250;
static char evt[buf_size];
static char event_data_buffer[buf_size];
static int c_step=0;
static float giw_dt=0;

struct Trep_info
{
    char Class;
    string dsc = "no description";
    string typ;
    string unit = "XX";
    string sys = "MPML";
    string uc;
};

//! Создание интерфейсного блока для MPML и привязка к переменным VPT
bool make_mpml_block(Tmpml_block_info& block, const char *struct_name, const char *type_name, Tstruct_p_result& r, char Class,map<string,Trep_info>& repmap)
{
    int block_len = r.type_size[type_name];
    block.resize(block_len);
    block.fill(0);
    
    auto &struct_info = r.structs[type_name];
    static map<string, int> type_mapper = { { "int", tIN }, 
    { "char", tCH },
    { "int32_t", tIN },
    { "int16_t", tIN },
    { "int8_t", tLO },
    { "short", tIN },
    { "bool", tLO }, 
    { "float", tRE }, 
    { "double", tRE } };
    for(auto &var: struct_info.pod_element_list)
    {
        void* ptr = block.data + var.offset;
        char var_name[256] = {0};
        //string var_name = string(struct_name) + "." + var.name;
        sprintf(var_name,"%s.%s",struct_name,var.name.c_str());
        if(ptr > block.end())
        {
            char errbuf[256] = {0};
            sprintf(errbuf,"Cannot add VPT '%s': out of memory block",var_name);
            pintegr.errPrintfT0(msFERR,errbuf);
            return false;
        }
        int typeID = type_mapper[var.vi->type_name];
        int ndims = var.vi->shape.size();
        int* shape = var.vi->shape.data();
        int var_el_size = var.vi->element_size();
        vector<desDim> dims(ndims);
        for(int i=0; i<ndims; ++i)
        {
            dims[i].lo = 0;
            dims[i].hi = shape[i];
        }
        if(!AddVPT(ptr, var_el_size, Class, typeID, ndims, dims.data(), var_name))
        {
            char errbuf[256] = {0};
            sprintf(errbuf,"Cannot add VPT '%s'",var_name);
            pintegr.errPrintfT0(msFERR,errbuf);
            return false;
        }
        Trep_info ri;
        ri.Class = Class;
        ri.typ = var.vi->type_name;
        repmap[var_name] = ri;
    }
    char msgbuf[256] = {0};
    sprintf(msgbuf,"Adding buffer %s of type %s",struct_name,type_name);
    pintegr.errPrintfT0(msINFO,msgbuf);
    return true;
}

void pt_rep(map<string,Trep_info >& repmap, const char* repFileName)
{
    static map<string, string> type_mapper = { { "int", "IN4" }, 
    { "char", "CH1" },
    { "int32_t", "IN4" },
    { "int16_t", "IN2" },
    { "int8_t", "IN1" },
    { "short", "IN2" },
    { "bool", "LO" }, 
    { "float", "RE4" }, 
    { "double", "RE8" } };
    ofstream repos(repFileName);
    repos<<"NAME	TYPE	UNIT	DESCR	CLASS	SYSTEM	UC"<<endl;
    for(auto &i: repmap)
    {
        auto &rep = i.second;
        repos<<i.first<<"\t"<<type_mapper[rep.typ]<<"\t"<<rep.unit<<"\t"<<rep.dsc<<"\t"<<rep.Class<<"\t"<<rep.sys<<"\t"<<rep.uc<<endl;
        repos.flush();
    }
    repos.flush();
    repos.close();
}

class Tgiw_mpml: public Tgiw_mdl
{

    typedef Tmodel mpml_model_t;
    typedef map<string, vector<string> > Tblkdsc;
    
    map<string, Tmpml_block_info> in_blocks;
    map<string, Tmpml_block_info> out_blocks;
    
    map<string,pair<char,Tmpml_block_info> > all_blocks;
    
    Tblkdsc in_blocks_dict; //!< map с именами in-структур и именами публикуемых экземпляров этих структур
    Tblkdsc out_blocks_dict; //!< map с именами out-структур и именами публикуемых экземпляров этих структур
    
    Tstruct_p_result r;

    void m_constructor();
    void m_decstructor();
    bool idrv();
    bool send_step=false;
    vector<Tgetput_el> getput_table_I; //Таблица для входящих в   GIW интерфейсов
    vector<Tgetput_el> getput_table_O; //Таблица для исходящих из GIW интерфейсов
    void create_getput_table();
    bool gpt_created = false;
    string getput_yaml;
    string server_config;
    map<string,Trep_info> repmap;
public:
    string name; // Имя модели для mpml-сервера
    int c_step;
    mpml_model_t * link = nullptr;
    Tgiw_mpml();
    ~Tgiw_mpml();
    void step(float *dt);
    void save(const char* file_name){};
    void load(const char* file_name);
    void looker(long *a, long *b);
    int doCommand(cmdBase& cmd);
    
    bool data_changed = false;
    bool model_loaded = false;
    bool state_loaded = false;
};

#include "thread_syncro.h"

bool Tgiw_mpml::idrv() //InitDriver: создаются буфера ZMQ и регистрируются переменные GIW
{
    using namespace idfp;
    YAML::Node node;
    
    char* config_yaml = pintegr.getCfgPath("MPML","MPML_CONFIG","",0,false);
    
    if(config_yaml == nullptr)
    {
        pintegr.errPrintfT0(msFERR,"MPML configuration file not found in MPML section (check MPML_CONFIG key)");
    }
    
    char* mpml_name = pintegr.getCfg("MPML","MPML_NAME",0);
    if(mpml_name == nullptr)
    {
        pintegr.errPrintfT0(msFERR,"MPML model name not found in MPML section (check MPML_NAME key)");
    }

    getput_yaml = pintegr.getCfgPath("MPML","GETPUT","",0,false);
    server_config = pintegr.getCfgPath("MPML","SRVCFG","",0,false);
    if(server_config.size() == 0)
    {
        pintegr.errPrintfT0(msFERR,"MPML server configuration file not found in MPML section (check the SRVCFG key)");
    }
    
    name = mpml_name;
    
    //Чтение конфигурации блоков данных для MPML
    try
    {
        node = YAML::LoadFile(config_yaml);
    }
    catch(...)
    {
        char cwd[FILENAME_MAX]={0};
        _getcwd(cwd, FILENAME_MAX);
        char errbuf[256];
        sprintf(errbuf,"MPML configuration file %s not found",config_yaml);
        pintegr.errPrintfT0(msFERR,errbuf);
        return false;
    }
    
    //Извлечение имени заголовочного файла С++ с описанием структур блоков двнных
    string iface_file = node["ifaceroot"].as<string>();
    
    //Извлечение словарей с блоками данных по направлению их передачи.
    //Ключами словарей являются типы структур блоков, элементы -- массивы блоков этого типа
    //Входящие блоки
    in_blocks_dict = node["in_names"].as<Tblkdsc>();
    //Исходящие блоки
    out_blocks_dict = node["out_names"].as<Tblkdsc>();
    send_step = node["mpml_send_step"].as<bool>();
    
    wchar_t *fileName;
    
    MPML_phase=MPML_phase_regvar_begin;
    
    try
    {
        
        //Парсинг заголовочного файла. В результате парсинга заполняется структура r, в которой содержится
        //информация о типах и переменных, содержащихся в заголовочном файле
        fileName = coco_string_create(iface_file.c_str());
        Scanner scanner = Scanner(fileName);
        Parser parser = Parser(&scanner);
        parser.r = &r;
        parser.Parse();
        if (parser.errors->count != 0)
        {
            cout << "num errors:" << parser.errors->count << endl;
            return false;
        }
        else
        {
            //Вычисление байтовых смещений от начала структур до их конечных элементов (поиском в глубину)
            r.calc_offsets(4);
            //Создание списков переменных для каждой структуры.
            //Списки переменных находятся в векторе r.structs["struct_name"].second.pod_element_list
            r.make_pod_element_list();
        }
        
    }
    catch(...)
    {
        cout<<"ошибка чтения файла "<<fileName<<endl;
        return false;
    }
    
    m_constructor();
    
    //Cоздание блоков памяти и привязка их к переменным VPT
    //Таблица ключей в конфигурации по классам переменных
    map<char,string> block_keys = {{'I',"in_names" },
                                   {'O',"out_names"}};
    repmap.clear();
    for(auto &bk: block_keys)
    {
        char Class = bk.first; //Первый элемент -- класс переменной (I,O,P,...)
        string key = bk.second; //Второй элемент -- ключ YAML
        auto blocks = node[key].as<Tblkdsc>(); //Списки переменных отсортированные по именам типов структуп
        for(auto &blk: blocks)
        {
            auto type_name = blk.first.c_str(); //Берем тип структуры
            for(auto &struct_name: blk.second) //Проходим по всем переменным для структуры типа type_name
            {
                //Добавляем в массив MPML блоков новую структуру класса Class c именем struct_name
                auto& it_block = all_blocks.insert(make_pair(struct_name,make_pair(Class,Tmpml_block_info())));
                if(it_block.second) //Если структура добавилась, то создаем переменные в модели
                {
                    Tmpml_block_info& mpml_block = it_block.first->second.second;
                    bool success_create = make_mpml_block(mpml_block,struct_name.c_str(),type_name,r,Class,repmap);
                    if(!success_create)
                    {
                        //Выводим сообщение об ошибке создания переменных для структуры
                        char errbuf[256];
                        sprintf(errbuf,"Cannot create VPT for struct %s %s",type_name,struct_name.c_str());
                        pintegr.errPrintfT0(msFERR,errbuf);
                        return false;
                    }
                    if (Class == 'I')
                    {
                        mpml_ivar(link, (char*)(struct_name.c_str()), mpml_block.data, mpml_block.len);
                        mpml_event(link, "step", (char*)(struct_name.c_str()));
                        mpml_event(link, "load", (char*)(struct_name.c_str()));
                    }
                    else
                    {
                        mpml_var(link, (char*)(struct_name.c_str()), mpml_block.data, mpml_block.len);
                    }
                }
                else //Если структуру не удалось добавить, выводим ошибку
                {
                    char errbuf[256];
                    sprintf(errbuf,"Cannot create buffer for struct %s %s",type_name,struct_name.c_str());
                    pintegr.errPrintfT0(msFERR,errbuf);
                    return false;
                }
            }
        }
    }
    char mnm[256]={0};
    //Регистрация соответствия возможных событий и переменных создаваемых моделью
    //mpml_event(link, "save", mnm);
    sprintf(mnm,"_%s",name.c_str());
    mpml_event(link, "load", mnm);
    mpml_event(link, "step", mnm);
    MPML_phase=MPML_phase_regvar_done;
    //Печать таблицы переменных
    pt_rep(repmap,"mpml.rep");
    
    //Ожидание когда зарегистрируются переменные коннекторов в enisey_spw
    HMODULE pyenisey_pyd = GetModuleHandle("pyensey.pyd");
    if(pyenisey_pyd != NULL)
    {
        typedef int(*f_int_t)();
        enisey_registered_in_MPML = (f_int_t)GetProcAddress(pyenisey_pyd,"enisey_registered_in_MPML");
        if(enisey_registered_in_MPML != NULL)
        {
            WaitForRegisterEniseyConnectors();
        }
    }
    
    //Регистрация модели на сервере
    MPML_phase=MPML_phase_reglink_begin;
    mpml_register(link);
    MPML_phase=MPML_phase_reglink_done;
    model_loaded = true;
    return true;
}

void Tgiw_mpml::m_constructor()
{
    if (link == nullptr)
    {
        try
        {
            char msg[256]={0};
            sprintf(msg,"Link model '%s'",name.c_str());
            pintegr.errPrintfT0(msINFO,msg);
            link = mpml_new_link_from_cfg(server_config.c_str(), name.c_str());
        }
        catch (...)
        {
            char msg[256]={0};
            sprintf(msg,"Linking model '%s': can't read MPML server configuration from file '%s'",name.c_str(),server_config.c_str());
            pintegr.errPrintfT0(msERR,msg);
        }
    }
}

void Tgiw_mpml::m_decstructor()
{
    mpml_send_event(link, "stop", nullptr, 0);
    mpml_stop_ns(link);
    if(link != nullptr)
    {
        mpml_delete_link(link);
    }
    model_loaded = false;
}

Tgiw_mpml::Tgiw_mpml()
{
    if(idrv())
    {
        VO(c_step, "c_step_model_mpml");
    }
    else
    {
        pintegr.errPrintfT0(msFERR,"Exception при создании объекта 'Tgiw_mpml' ");
        throw std::exception("Error in Tgiw_mpml constructor");
    }
}

Tgiw_mpml::~Tgiw_mpml()     
{
    m_decstructor();
}

void Tgiw_mpml::step(float *dt)
{
    double dt1 = *dt;
    c_step += 1;
    //cout<<"Model "<<name<<" Step: "<<c_step<<endl;
}

void Tgiw_mpml::load(const char* file_name)
{
    
}

Tgiw_mdl * Mmodel()
{
    static Tgiw_mpml mdl;
    return &mdl;
}

extern "C" Tmodel* get_MPML_link()
{
    return Mmodel()->link;
}

void Tgiw_mpml::looker(long *a, long *b)
{
    if(*a==2)
    {
    }
}

int Tgiw_mpml::doCommand(cmdBase& cmd)
{
    extern const float& get_giw_deltat();
    extern const double& get_giw_time();
    if(cmd.code == cStartStep)
    {
        //Для соблюдения строгой синхронности расчетов моделей весь обмен данными между ними должен выполняться
        //перед началом шага модели.
        //Обмен данными состоит из двух частей: сетевого и локального
        //Локальный обменн данными присходит между буферами MPML и переменными GIW.
        //Для этого создается таблица копирования getput_table, в которой содержатся пара переменных и функция
        //копирования.
        if(send_step)
        {
            double Dt=get_giw_deltat();
            mpml_send_event(link,"step",(char*)&Dt,sizeof(Dt));
        }
        int data_size = 0;
        const int evt_buf_size = 256;
        char evt[evt_buf_size]={0};
        char event_data_buffer[evt_buf_size]={0};
        data_size = mpml_get_cmd(link, evt, buf_size, event_data_buffer, buf_size);
    
        if(strncmp(evt,"step",max_cmd_size)==0)
        {
            try
            {
                mpml_data_exchange(link, evt); //Передача буферов в сеть
                data_changed = true;
            }
            catch(zmq::error_t& e)
            {
                int errcode = e.num();
                char errbuf[200] = {0};
                sprintf(errbuf,"Caught some exception in mpml_data_exchange. Error code is %x",errcode);
                pintegr.errPrintfT0(msINFO,errbuf);
                pintegr.errPrintfT0(msFERR,e.what());
            }
        }
        else
        {
            char errbuf[200] = {0};
            sprintf(errbuf,"Caught invalid event '%s' instead of 'step' at model time %f",evt,get_giw_time());
            pintegr.errPrintfT0(msINFO,errbuf);
        }
        if(data_changed)
        {
            if(!gpt_created) //Создание таблицы копирования
            {
                create_getput_table();
            }
            if(gpt_created) //Локальное копирование переменных
            {
                for(auto& i: getput_table_I)
                {
                    i();
                }
            }
            //cout<<"Model "<<name<<" cStartStep: "<<c_step<<endl;
            //cout.flush();
        }
    }
    if(cmd.code == cSyncroData)
    {    
        if(data_changed)
        {
            if(!gpt_created) //Создание таблицы копирования
            {
                create_getput_table();
            }
            if(gpt_created) //Локальное копирование переменных
            {
                for(auto& i: getput_table_O)
                {
                    i();
                }
            }
            //cout<<"Model "<<name<<" cSyncroData: "<<c_step<<endl;
            //cout.flush();
            data_changed = false;
        }
    }
    if(cmd.code == cLoadDone) //Синхронизация буферов с загруженным состоянием
    {
        if(!gpt_created) //Создание таблицы копирования
        {
            create_getput_table();
        }
        if(gpt_created) //Локальное копирование переменных
        {
            for(auto& i: getput_table_O)
            {
                i();
            }
        }
    }
    if(cmd.code == cStop)
    {
        //int data_size = 0;
        //const int evt_buf_size = 256;
        //char evt[evt_buf_size]={0};
        //char event_data_buffer[evt_buf_size]={0};
        //if(!data_changed && model_loaded)
        //{
        //    data_size = mpml_get_cmd(link, evt, buf_size, event_data_buffer, buf_size);
        //    mpml_data_exchange(link, evt); //Передача буферов в сеть
        //    data_changed = true;
        //}
    }
    return 0;
}

template<class T>
void yaml_node_put(YAML::Node& n, void* buf)
{
    *(T*)buf = n.as<T>();
}

typedef void (*yaml_val_filler_t)(YAML::Node& n, void* buf);

#define RETURN_YAML_NODE_PUT(typ,TYP) \
  if(strcmp(typ,#TYP) == 0)\
  {\
      return yaml_node_put<TYP>;\
  }

yaml_val_filler_t yaml_val_filler(const char* typ)
{
    RETURN_YAML_NODE_PUT(typ,bool)
    RETURN_YAML_NODE_PUT(typ,char)
    RETURN_YAML_NODE_PUT(typ,short)
    RETURN_YAML_NODE_PUT(typ,int)
    RETURN_YAML_NODE_PUT(typ,float)
    RETURN_YAML_NODE_PUT(typ,double)
    return nullptr;
}

#include "yaml_templ/yaml_templ.h"
//!Построение таблицы локального копирования переменных GIW в буфера MPML
void Tgiw_mpml::create_getput_table()
{
    getput_table_I.clear();
    getput_table_O.clear();
    if(getput_yaml.size()>0)
    {
        YAML::Node node;
        try
        {
            node = YAML::Load(YAML_templ::parse(getput_yaml.c_str()));
        }
        catch(...)
        {
            char err[200] = {0};
            sprintf(err,"Cannot read interface YAML file: %s", getput_yaml.c_str());
            pintegr.errPrintfT0(msERR,err);
        }
        for(auto &i: node)
        {
            string name_to = i.first.as<string>();
            YAML::Node info = i.second;
            string name_from;
            YAML::Node name_from_node = info["input"];
            if(name_from_node)
            {
                name_from = name_from_node.as<string>();
            }
            else if(name_from_node = info["I"])
            {
                name_from = name_from_node.as<string>();
            }
            else
            {
                char err[200] = {0};
                sprintf(err,"Cannot find input for output variable %s. Skipping",name_to.c_str());
                pintegr.errPrintfT0(msWARN,err);
                continue;
            }

            Tgetput_el el;
            el.from = nm_fnd(name_from.c_str());
            if(el.from == nullptr)
            {
                char err[200] = {0};
                sprintf(err,"Cannot find variable %s. Skipping",name_from.c_str());
                pintegr.errPrintfT0(msWARN,err);
                continue;
            }
            el.to = nm_fnd(name_to.c_str());
            if(el.to == nullptr)
            {
                char err[200] = {0};
                sprintf(err,"Cannot find variable %s. Skipping",name_to.c_str());
                pintegr.errPrintfT0(msWARN,err);
                continue;
            }
            const char* typ_from = get_ctype(el.from);
            if(typ_from == nullptr)
            {
                char err[200] = {0};
                sprintf(err,"Cannot find C type for variable %s. Skipping",name_from.c_str());
                pintegr.errPrintfT0(msWARN,err);
                continue;
            }
            const char* typ_to = get_ctype(el.to);
            if(typ_to == nullptr)
            {
                char err[200] = {0};
                sprintf(err,"Cannot find C type for variable %s. Skipping",name_to.c_str());
                pintegr.errPrintfT0(msWARN,err);
                continue;
            }
            char tyconv[256] = {0};
            sprintf(tyconv,"%s->%s",typ_from,typ_to);
            el.assign = getput(tyconv);

            YAML::Node nd_lint = info["scale"];
            el.scale = nullptr;
            el.shift = nullptr;
            if(nd_lint)
            {
                auto f = yaml_val_filler(typ_from);
                if(f != nullptr)
                {
                    f(nd_lint,el.scale_buf);
                    el.scale = (void*)(&(el.scale_buf[0]));
                }
            }
            nd_lint = info["shift"];
            if(nd_lint)
            {
                auto f = yaml_val_filler(typ_from);
                if(f != nullptr)
                {
                    f(nd_lint,el.shift_buf);
                    el.shift = (void*)(&(el.shift_buf[0]));
                }
            }
            if(el.check())
            {
                //Если переменная находится в итерфейсном буфере, то перекладывание идет из модели GIW в буфер
                if(repmap.find(name_to) != repmap.end())
                {
                    getput_table_O.push_back(el);
                }
                else
                {
                    getput_table_I.push_back(el);
                }
            }
            else
            {
                char err[200] = {0};
                sprintf(err,"Cannot create link %s -> %s, types %s",name_from.c_str(),name_to.c_str(),tyconv);
                pintegr.errPrintfT0(msWARN,err);
            }
        }
    }
    gpt_created = true;
}