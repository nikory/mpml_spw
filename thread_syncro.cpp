#include "thread_syncro.h"


static std::atomic<int> MPML_phase(MPML_phase_init);
static int (*enisey_registered_in_MPML)(void)=nullptr;

//Функция возвращает флаг состояния алгоритма регистрации в MPML
extern "C" int get_MPML_phase()
{
    return MPML_phase;
}

#include <thread>
#include <chrono>
//Функция выполняется пока в enisey_spw.dll не завершится регистрация в MPML интерфейсных структур коннекторов
void WaitForRegisterEniseyConnectors()
{
    while(enisey_registered_in_MPML() == 0)
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }
}
