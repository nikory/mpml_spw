//������ ��������� ��� ������ ������ �� ������ ������-��������
struct Tdata
{
 int x;
};

//TRANSMITTERS
//��������� �������� �� ������
struct Ttrans_loop
{
    double HOT_LEG_T,   //����������� ������� �����, �
           COLD_LEG_T,  //����������� �������� �����, �
           SG_LEVEL_M,  //������� � ��, �
           SG_IN_G,     //������ ����������� ����, ��/�
           SG_OUT_G,    //������ ������� ����, ��/�
           SG_OUT_P,    //�������� �� ������ �� ��, ��
           SG_OUT_T,    //����������� ���� �� ������ �� ��, �
           MCP_IN_P,    //�������� �� ����� ���, ��
           MCP_OUT_P,   //�������� �� ������ ���, ��
           MCP_MM       //������ �� ���� ���
           ;
};

//�������� �������� �������� ���������� ��
struct Ttrans
{
    double KD_LEVEL_M,  //�������� ������� � ��, �
           KD_TL,       //����������� � ��
           POUT_CORE,   //�������� �� ������ �� �.�., ��
           MSH_P,       //�������� � ���, ��
           KBA_IN_G,    //������ ��������, �/�
           KBA_OUT_G    //������ ��������, �/�
           ;
    Ttrans_loop LOOP[4];//������� �� ������
};

//CONTROL ENGINES
//��������� ������ ���
struct TKD_heater_grp
{
    int8_t KEY;         //���� ��������� ���, {ON = true, OFF = false}
    double POW;         //�������� ������ ���, ��
};

//��������� ��� ���������� ���������
struct Tvalve
{
    double POS;         //��������� ��������, �.�. (0 < POS < 1)
};

//��������� ���������� ���������� �������
struct Tpump
{
    double OMEGA;       //������� ��������, ��/���
};

//��������� ����������� ���������� ��������� �����
struct Tcontrols_loop
{
    Tpump MCP;          //���������� ����
    Tvalve BRUA;        //���������� ���-�
    Tvalve FWVLV;       //��������� ����������� ���� ��
    Tvalve TURVLV;      //��������� �������� ���� ����� ��������
};

//��������� ����������� ���������� ������
struct Tcontrols
{
    Tvalve KBA_IN_VLV;          //�������� �������� ������� �������
    Tvalve KBA_OUT_VLV;         //�������� �������� ������� �������
    Tvalve RKKD;                //�������� �������
    Tvalve BZZ[2];              //�������������� �������� �������
    TKD_heater_grp TEN[5];      //������ ��� ��
    Tvalve BRUK[8];             //������� ���-�
    Tcontrols_loop LOOP[4];     //���������� �������
};

