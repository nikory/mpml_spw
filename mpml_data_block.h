#ifndef MPML_DATA_BLOCK_H
#define MPML_DATA_BLOCK_H

struct Tmpml_block_info
{
    char* data = nullptr;
    int len;
    Tmpml_block_info(int nbytes = 0, char filler=0);
    Tmpml_block_info(const Tmpml_block_info&);
    ~Tmpml_block_info();
    void resize(int new_len);
    void fill(char x);
    void copy(const Tmpml_block_info&);
    void* end() const;
};

Tmpml_block_info::Tmpml_block_info(int nbytes, char filler)
{
    resize(nbytes);
    fill(filler);
}

Tmpml_block_info::Tmpml_block_info(const Tmpml_block_info& pat)
{
    resize(pat.len);
    copy(pat);
}

Tmpml_block_info::~Tmpml_block_info()
{
    if(data != nullptr)
    {
        delete [] data;
    }
}

void Tmpml_block_info::resize(int new_len)
{
    if(new_len < 1)
    {
        len = 0;
        if(data != nullptr)
        {
            delete [] data;
            data = nullptr;
        }
    }
    else if(new_len != len)
    {
        len = new_len;
        if(data != nullptr)
        {
            delete [] data;
        }
        data = new char[new_len];
    }
}

void Tmpml_block_info::fill(char filler)
{
    if(data != nullptr)
    {
        memset(data,filler,len);
    }
}

void Tmpml_block_info::copy(const Tmpml_block_info& pat)
{
    int ncpy = (len<pat.len) ? (len) : (pat.len);
    memcpy(data,pat.data,ncpy);
}

void* Tmpml_block_info::end() const
{
    return data + len;
}

#endif